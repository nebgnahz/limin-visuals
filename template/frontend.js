var sid = new Vue({
  el: '#spreadsheet',
  data: {
    sid: "1D1iKRCZlJ162wXB9MtoPvRYxde_EWbFyVuvlUZliC28",
  },
  computed: {
    url: function () {
      // `this` points to the vm instance
      return "https://spreadsheets.google.com/feeds/cells/" + this.sid +
        "/1/public/values?alt=json";
    }
  },
  methods: {
    fetchData: function () {
      var xhr = new XMLHttpRequest()
      xhr.open("GET", this.url)
      xhr.onreadystatechange = function(event) {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            updateFromSpreadsheetData(JSON.parse(xhr.responseText));
          } else {
            alert("Double check your spreadsheet id");
          }
        }
      };
      xhr.send(null);
    }
  }
})

sid.$watch('url', function (val) {
  this.fetchData();
})

function updateFromSpreadsheetData(data) {
  var data = data.feed.entry;
  // TODO: Then parse the data
}

// Load data
sid.fetchData();

function codeUpdated() {
  editor.getSession().getValue();
}

function run() {
  var code = editor.getSession().getValue();
  eval(code);
}

// Hack from http://stackoverflow.com/questions/28930846/
var style = (function() {
    // Create the <style> tag
    var style = document.createElement("style");
    // WebKit hack
    style.appendChild(document.createTextNode(""));
    // Add the <style> element to the page
    document.head.appendChild(style);
    console.log(style.sheet.cssRules); // length is 0, and no rules
    return style;
})();

style.sheet.insertRule('#chart div { font: 10px sans-serif; background-color: steelblue; text-align: right; padding: 3px; margin: 1px; color: white;}', 0);
console.log(style.sheet.cssRules); // length is 1, rule added
