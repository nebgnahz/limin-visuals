var title = "2016 Q1 Edtech";

var bubble = {
    name: "公司",
    x: "行业细分",
    y: "主营业务",
    t: "商业模式",
    z: "融资金额（万usd）"
};

var columns = ["3", "7", "8", "9", "12"];

var x = ["学前", "K12", "高等教育", "STEM", "综合", "职业技能", "兴趣"];
var y = ["工具", "社区", "内容", "服务", "综合", "平台"];

var factor = 3;

function showData(json) {
    var data = json.feed.entry;
    var table = [];
    var rowData = [];
    var ncol;
    var header = false;

    for (var r = 0; r < data.length; r++) {
        var cell = data[r]["gs$cell"];
        var val = cell["$t"];

        if (cell.col == 1 && rowData.length > 0) {
            table.push(rowData);
            rowData = [];
            header = true;
        }

        var jitter = (Math.random() - 0.5) / factor;
        if (!header) {
            if (columns.indexOf(cell.col) !== -1) {
                rowData.push(val);
            }
        } else {
            if (cell.col == columns[0]) {            // name
                rowData.push(val);
            } else if (cell.col == columns[1]) {     // x
                rowData.push(x.indexOf(val) + 1 + jitter);
            } else if (cell.col == columns[2]) {     // y
                rowData.push(y.indexOf(val) + 1 + jitter);
            } else if (cell.col == columns[3]) {     // t
                rowData.push(val);
            } else if (cell.col == columns[4]) {     // z
                var z = parseInt(val);
                if (isNaN(z)) {
                    // undefined value
                    rowData.push(0);
                } else {
                    rowData.push(z);
                }
            }
        }
    }
    d = table;
}

google.charts.load('current', {'packages':['corechart']});
google.charts.setOnLoadCallback(drawSeriesChart);

var d = [
    ['公司', '行业细分', '主营业务', '商业模式',     '融资金额（万usd）'],
    ['CAN',    80.66,              1.67,      'North America',  33739900],
    ['DEU',    79.84,              1.36,      'Europe',         81902307],
    ['DNK',    78.6,               1.84,      'Europe',         5523095],
    ['EGY',    72.73,              2.78,      'Middle East',    79716203],
    ['GBR',    80.05,              2,         'Europe',         61801570],
    ['IRN',    72.49,              1.7,       'Middle East',    73137148],
    ['IRQ',    68.09,              4.77,      'Middle East',    31090763],
    ['ISR',    81.55,              2.96,      'Middle East',    7485600],
    ['RUS',    68.6,               1.54,      'Europe',         141850000],
    ['USA',    78.09,              2.05,      'North America',  307007000]
];

function drawSeriesChart() {
    var data = google.visualization.arrayToDataTable(d);

    var hticks = [];
    hticks.push({ v: 0, f: "" });
    for (var i = 0; i < x.length; i++) {
        hticks.push({ v: i + 1, f: x[i] });
    }
    hticks.push({ v: x.length + 1, f: "" });

    var vticks = [];
    vticks.push({ v: 0, f: "" });
    for (var i = 0; i < y.length; i++) {
        vticks.push({ v: i + 1, f: y[i] });
    }
    vticks.push({ v: y.length + 1, f: "" });

    var options = {
        title: title,
        hAxis: { title: '行业细分', ticks: hticks },
        vAxis: { title: '主营业务', ticks: vticks },
        bubble: {
            opacity: 0.7,
            textStyle: {
                opacity: 0,
            },
        },
        legend: {
            position: 'top',
        }
    };

    var chart = new google.visualization.BubbleChart(
        document.getElementById('series_chart_div'));

    chart.draw(data, options);
}
